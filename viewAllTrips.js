let TRIPS_DATA_KEY = "TripList"


function updateLocalStorage(data){
    localStorage.setItem(TRIPS_DATA_KEY, JSON.stringify(data))//save the lockers
}


function dateData() {
	return function(a, b) {
		var value1 = a["_time"];
		var value2 = b["_time"];
		return Date.parse(value1) - Date.parse(value2);
	}
}

//retrieving data from local storage at the key LOCKER_DATA_KEY
function getDataLocalStorage(){
    return JSON.parse(localStorage.getItem(TRIPS_DATA_KEY));
}

function displayTrips(data){
    for(let i = 0; i < data.length; i++){
        let name = data[i]._name;
        let time = data[i]._time
        let origin = data[i]._origin
        let final = data[i]._final
        let stops = data[i]._stops
        let distance = data[i]._distance
        
        
        let string = '<div class="mdl-cell mdl-cell--9-col"><div class="a" style="background-color:#e3d5d5"><div class="mdl-card__title mdl-card--expand"><h1 class="mdl-cell mdl-cell--5-col" style="font-size: 70px">' + name + '</h1><h5>&nbsp;' + time + '<div class="mdl-card__actions mdl-card--border"><div class="mdl-layout-spacer"></div></br>Origin: ' + origin + '</br></br>Final: ' + final+ '</br></br>Total stops: ' + stops + '</br></br>Total distance: ' + distance + '</div></div></h5><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="view(' + i +')" style="font-size: 15px; margin-left: 20px">View Details</a></div></div>'
        
       /* '<div class="mdl-cell mdl-cell--9-col"><div class="a" style="background-color:#e3d5d5"><div class="mdl-card__title mdl-card--expand"><h1 class="mdl-cell mdl-cell--5-col">' + name + '</h1><h5>&nbsp;' + date + '<div class="mdl-card__actions mdl-card--border"><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="view(' + i +')">View Details</a><div class="mdl-layout-spacer"></div>Origin: ' + origin + '</br></br>Final: ' + final+ '</br></br>Total stops: ' + stops + '</br></br>Total distance: ' + distance + '</div></div></h5></div></div>'*/
        
        /*'<div class="mdl-cell mdl-cell--9-col"><div class="mdl-card mdl-shadow-2dp locker" style="background-color:#e3d5d5"><div class="mdl-card__title mdl-card--expand"><h1>' + name + '</h1><h5>&nbsp;' + date + '</h5></div><div class="mdl-card__actions mdl-card--border"><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="view(' + i +')">View Details</a><div class="mdl-layout-spacer"></div>Origin: ' + origin + '</br></br>Final: ' + final+ '</br></br>Total stops: ' + stops + '</br></br>Total distance: ' + distance + '</div></div></div>'*/
        //dynamic generate the html element
        document.getElementById("tripsTable").innerHTML += string
    }
}


//use to check function displayTrips(data)
let data = [{
    _name: "name",
    _time: "2019-04-26 11:05",
    _origin: "MEL airport",
    _final: "SYD airport",
    _stops: 2,
    _distance: 2999
},
        {
    _name: "name",
    _time: "2019-04-26 11:06",
    _origin: "MEL airport",
    _final: "SYD airport",
    _stops: 2,
    _distance: 2999
},
        {
    _name: "name",
    _time: "2019-04-24 11:05",
    _origin: "MEL airport",
    _final: "SYD airport",
    _stops: 2,
    _distance: 2999
},
         {
    _name: "name",
    _time: "2019-05-26 11:05",
    _origin: "MEL airport",
    _final: "SYD airport",
    _stops: 2,
    _distance: 2999
}]

let sorted = data.sort(dateData())
displayTrips(sorted)
updateLocalStorage(sorted)






